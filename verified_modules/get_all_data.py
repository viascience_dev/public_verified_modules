# Copyright 2018 Via Science, Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import logging
import os
from typing import List
from psycopg2 import sql


def main(database, table: sql.Composed, result_file_path: str = None):
    """
    Get data from a list of columns, and save to a csv file.

    Data from a postgres table is saved to a csv using comma-separated columns and newlines as row delimiters.
    Double quotes are placed around strings as-needed, for example, if there are commas or newlines within the field.
    A \ is used as the escape character, for example, when the string itself contains a double quote.

    :param database: database connection object
    :param table:  psycopg2.sql.Composed object representing a virtual table containing the data (required)
    :param result_file_path: path to save the csv
    :return: the path to the file
    :raises TypeError: If one or more of the required arguments is missing
    :raises psycopg2.ProgrammingError: If there is an error executing the requested query on the database.
    """
    if not (isinstance(table, sql.Composed) or isinstance(table, sql.Identifier)):
        message = f'get_all_data.main: expected table to be a sql.Composed object, received {table}'
        logging.error(message)
        raise TypeError(message)

    if not isinstance(result_file_path, str):
        message = f'get_all_data.main: expected result_file_path to be a string, received {result_file_path}'
        logging.error(message)
        raise TypeError(message)

    results_dir = os.path.dirname(result_file_path)
    if results_dir == "":
        results_dir = "./"

    if not os.path.isdir(results_dir):
        message = f'get_all_data.main: required directory {results_dir} for saving results does not exist.'
        logging.error(message)
        raise FileNotFoundError(message)

    command = sql.SQL("COPY (SELECT * FROM {}) TO STDOUT WITH CSV ESCAPE '\\'").format(table)
    cursor = database.cursor()
    with open(result_file_path, "w") as f:
        cursor.copy_expert(command, f)

    return result_file_path
