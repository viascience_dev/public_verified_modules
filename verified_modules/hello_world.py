import logging
from psycopg2 import sql


def main(db, table, **kwargs):
    """Executes some simple database queries and returns 'Hello world!'"""
    schemas = db.execute("SELECT schema_name FROM information_schema.schemata").fetchall()
    logging.debug(f"schemas={schemas}")

    result = db.execute(sql.SQL("EXPLAIN ANALYZE SELECT * FROM {};").format(table,)).fetchall()
    logging.debug(f"result={result}")

    logging.debug(f"Received keyword arguments: kwargs={kwargs}")

    return "Hello world!"
