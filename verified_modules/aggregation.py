# Copyright 2018 Via Science, Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


from typing import List, Dict
import logging
import psycopg2
from psycopg2 import sql
from datetime import datetime

INTERVAL_CHOICES = ['microseconds', 'milliseconds',
                    'second', 'minute', 'hour',
                    'day', 'week', 'month',
                    'quarter', 'year', 'decade',
                    'century', 'millennium']


def build_query(
        table: sql.Composed = None,
        columns: List[str] = None,
        exponent: str = None,
        time: str = None,
        interval: str = None,
        labels: Dict = None,
):
    """Build a SQL query that will return the  moments of a column or columns.

    This function is called from the main() function in this script.
    It creates a psycopg2 object and a list of parameters, allowing the construction of
    a parametrized query to protect against SQL injection.

    The query returned is equivalent to:
    SELECT date_trunc(interval, time), AVG(col1), AVG(col2), ... AVG(colN) FROM test
      WHERE (label_col1 = label1 AND label_col2 = label2, AND ...)
      GROUP BY date_trunc('day', t)

    :param table: psycopg2.sql.Composed object representing a virtual table containing the data (required)
    :param columns: a column name or list of column names (required)
    :param exponent: integer exponent (required)
    :param time: name of the column that represents a time stamp
    :param interval: the time interval over which to aggregate data.
            Must be one of:
            ['microseconds', 'milliseconds',
            'second', 'minute', 'hour',
            'day', 'week', 'month',
            'quarter', 'year', 'decade',
            'century', 'millennium']
            ignored if time_column is None.
    :param labels: dict of column names and associated labels to use in where statement
    :return: psycopg2.sql.Composed object and a tuple of parameters to pass to the query
    :raises TypeError: If the passed parameters are not the expected types
    :raises ValueError: If the selected time interval is not in the list of supported values
    """
    exponent_mapper = {"mean": 1, "variance": 2, "skewness": 3, "kurtosis": 4}
    try:
        exponent = exponent_mapper[exponent]
    except KeyError:
        message = f"aggregation.build_query: expected exponent in {list(exponent_mapper.keys())}, received {exponent}."
        raise ValueError(message)

    if not isinstance(table, sql.Composed):
        raise TypeError(f'aggregation.build_query: expected table to be a sql.Composed object, received type {type(table)}')

    if isinstance(columns, List):
        params = [exponent] * len(columns)
    elif isinstance(columns, str):
        columns = [columns]
        params = [exponent]
    else:
        raise TypeError(f'aggregation.build_query: expected column name or list of column names, received {columns}')

    select_list = [sql.SQL("AVG({}^({}))").format(sql.Identifier(c), sql.Placeholder()) for c in columns]

    labels_list = []
    if labels is not None:
        try:
            label_columns = labels.keys()
        except AttributeError:
            raise TypeError(f'aggregation.build_query: expected dictionary for label columns, received {labels}')
        if not all([isinstance(c, str) for c in label_columns]):
            raise TypeError(f"aggregation.build_query: expected column names for label columns, received {label_columns}")
        labels_list = labels_list + [sql.SQL("{}={}").format(sql.Identifier(c), sql.Placeholder()) for c in label_columns]
        query_where = sql.SQL(" AND ").join(labels_list)
        query_where = sql.SQL(" WHERE {}").format(query_where)
        params = params + [labels[c] for c in label_columns]
    else:
        query_where = None

    if isinstance(time, str) and isinstance(interval, str):
        if interval in INTERVAL_CHOICES:
            logging.info("Constructing the time aggregation part of the query.")
            time_trunc_query = (sql.SQL("date_trunc({},{})").format(sql.Placeholder(), sql.Identifier(time)))
            select_list = [time_trunc_query] + select_list
            query_group = sql.SQL(" GROUP BY {}").format(time_trunc_query)
            params = [interval] + params + [interval]
        else:
            message = f'aggregation.build_query: expected one interval in {INTERVAL_CHOICES}, received {interval}'
            raise ValueError(message)
    else:
        query_group = None

    query_part = sql.SQL(", ").join(select_list)
    query = sql.SQL("SELECT {} FROM {}").format(query_part, table)

    if query_where:
        query = query + query_where

    if query_group:
        query = query + query_group

    query = query + sql.SQL(";")
    return query, params


def build_query_distinct_labels(table: sql.Composed, label_columns: List[str]):
    label_columns = [sql.Identifier(c) for c in label_columns]
    query = sql.SQL("SELECT DISTINCT ") + sql.SQL(", ").join(label_columns)
    query = query + sql.SQL("FROM {};").format(table)
    return query


def main(database,
         table: sql.Composed,
         columns: List[str] = None,
         exponent: List[str] = None,
         time: List[str] = None,
         interval: List[str] = None,
         group: List[str] = None):
    """
    Query a PostgreSQL database to get moments of given columns.

    SELECT AVG(col1^exponent), AVG(col2^exponent), ..., AVG(colN^exponent)
    FROM table GROUP BY group_by_col1, group_by_col2, ... date_trunc(interval,time_column)

    Note: Null values are ignored.

    :param database: database connection object
    :param table:  psycopg2.sql.Composed object representing a virtual table containing the data (required)
    :param columns: a column name or list of column names (required), passed as a list
    :param exponent: string representing which moment to calculate, passed as a list of length one
    :param time: name of the column that represents a time stamp, passed as a list of length one
    :param interval: the time interval over which to aggregate data, passed as a list of length one;
            should be one of:
            ['microseconds', 'milliseconds',
            'second', 'minute', 'hour',
            'day', 'week', 'month',
            'quarter', 'year', 'decade',
            'century', 'millennium']
            ignored if time_column is None.
    :param group: list of columns to group by
    :return: query result, as a list or list of tuples
    :raises TypeError: If one or more of the required arguments is missing
    :raises ValueError: If a list of more than one element is passed for exponent, time, or interval
    :raises psycopg2.ProgrammingError: If there is an error executing the requested query on the database.
    """
    if table is None:
        message = f'aggregation.main: missing required keyword argument table'
        logging.error(message)
        raise TypeError(message)

    if not isinstance(table, sql.Composed):
        message = f'aggregation.main: expected table to be a sql.Composed object, received type {type(table)}'
        logging.error(message)
        raise TypeError(message)

    if columns is None:
        message = f'aggregation.main: missing required keyword argument columns'
        logging.error(message)
        raise TypeError(message)

    if exponent is None:
        message = f'aggregation.main: missing required keyword argument exponent'
        logging.error(message)
        raise TypeError(message)

    if len(exponent) > 1:
        message = f"aggregation.main: expected value for exponent, received {exponent}."
        logging.error(message)
        raise ValueError(message)

    exponent = exponent[0]

    if isinstance(time, List):
        if len(time) > 1:
            message = f"aggregation.main: expected one time column, received {time}."
            raise ValueError(message)
        elif len(time) == 1:
            time = time[0]
        else:
            pass
    elif time is None:
        time = []
    else:
        message = f"aggregation.main: expected a list for time, received {time}."
        raise ValueError(message)

    if isinstance(interval, List):
        if len(interval) > 1:
            message = f"aggregation.main: expected one interval, received {interval}."
            raise ValueError(message)
        elif len(interval) == 1:
            interval = interval[0]
        else:
            pass
    elif interval is None:
        interval = []
    else:
        message = f"aggregation.main: expected a list for interval, received {interval}."
        raise ValueError(message)

    if isinstance(group, str):
        group = [group]
    elif isinstance(group, List) or group is None:
        pass
    else:
        message = f"aggregation.main: expected a list for group, received {group}."
        raise TypeError(message)

    if group:
        logging.info("Getting distinct sets of labels for the group columns.")
        query = build_query_distinct_labels(table, group)
        cursor = database.execute(query)
        distinct_labels = cursor.fetchall()
    else:
        distinct_labels = []

    if not distinct_labels:
        q, query_params = build_query(
            table=table, columns=columns, exponent=exponent,
            time=time, interval=interval
        )
        cursor = database.execute(q, query_params)
        query_result = cursor.fetchall()
        query_result = [list(i) for i in query_result]
        if isinstance(query_result[0][0], datetime):
            for i in range(len(query_result)):
                query_result[i][0] = str(query_result[i][0])

        result = [query_result]

    else:
        result = []
        logging.info(f'aggregation.main: Executing query for each distinct set of labels.')
        for labels in distinct_labels:
            label_dict = {c: label for c, label in zip(group, labels)}
            q, query_params = build_query(
                table=table, columns=columns, exponent=exponent,
                time=time, interval=interval, labels=label_dict
            )
            try:
                cursor = database.execute(q, query_params)
            except psycopg2.ProgrammingError as exp:
                message = f"Error executing query: '{q}' with parameters: {query_params}."
                logging.error(message)
                raise exp

            query_result = cursor.fetchall()

            query_result = [list(i) for i in query_result]
            if isinstance(query_result[0][0], datetime):
                for i in range(len(query_result)):
                    query_result[i][0] = str(query_result[i][0])

            result.append(query_result)

    return result
