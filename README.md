# Verified Scripts

The verified scripts in this repository are for use in TAC.
Each script contains a function `main` which returns the result of the computation.
Users of TAC can authorize these scripts when they create an analytics contract.
The TAC platform will import and execute the `main` function of authorized scripts.
The parameter possibilities for each script are specified in a json file with the same name as the script.
These parameter possibilities are displayed in the TAC user interface during selection of the algorithm.
